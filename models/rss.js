// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var rssSchema = new Schema({
  url: { type: String, required: true, unique: true },
  link: { type: String, required: true, unique: true },
  title: { type: String },
  desc: { type: String },
  pubDate: { type: Date },
  ttl: { type: Number },
  cat: [],
  active: { type: Boolean, default: true },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

// the schema is useless so far
// we need to create a model using it
var Rss = mongoose.model('Rss', rssSchema);

// make this available to our users in our Node applications
module.exports = Rss;