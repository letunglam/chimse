// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var itemSchema = new Schema({
  link: { type: String, required: true, unique: true },
  title: { type: String, index: true },
  summary: { type: String },
  description: { type: String },
  date: { type: Date },
  dactive: { type: Boolean, default: true },
  dpoint: { type: Number },
  ditem: [],
  wactive: { type: Boolean, default: true },
  wpoint: { type: Number },
  witem: [],
  mactive: { type: Boolean, default: true },
  mpoint: { type: Number },
  mitem: [],  
  parent: { type: mongoose.Schema.Types.ObjectId, ref: 'rss'},
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

itemSchema.index({ title: 'text' });

// the schema is useless so far
// we need to create a model using it
var Item = mongoose.model('Item', itemSchema);

// make this available to our users in our Node applications
module.exports = Item;