var express = require('express');
var async = require('async');
var moment = require('moment');
var router = express.Router();

// models
var rss = require( '../models/rss' );
var item = require( '../models/item' );

/* GET Homepage. */
router.get('/', function(req, res, next) {
  item.aggregate([
    { 
      "$match": { 
        "date" : { 
          "$gt" : moment().startOf('day').toDate(),
          "$lt" : moment().endOf('day').toDate()
        },
        "dactive": true
      } 
    },
    {
      "$project": { 
        "title": 1, 
        "date": 1,
        "link": 1,
        "point": "$dpoint",
        "items": "$ditem",
        "parent": 1,
      }
    },
    {$sort: { point : -1}}
  ])
  .limit(30)
  .exec(function(err, docs){
    rss.populate(docs, {path: "parent"}, function(err){
      console.log(docs);
      res.render('index', { 
        title: 'Express',
        docs: docs
      });
    });
  })
});

/* GET Week News. */
router.get('/week', function(req, res, next) {
  item.aggregate([
    { 
      "$match": { 
        "date" : { 
          "$gt" : moment().subtract(7, 'days').startOf('day').toDate(),
          "$lt" : moment().endOf('day').toDate()
        },
        "mactive": true
      } 
    },
    {
      "$project": { 
        "title": 1, 
        "date": 1,
        "link": 1,
        "point": "$wpoint",
        "items": "$witem",
      }
    },
    {$sort: { point : -1}}
  ])
  .limit(30)
  .exec(function(err, docs){
    console.log(docs);
    res.render('index', { 
      title: 'Week',
      docs: docs
    });
  })
});

/* GET Months. */
router.get('/month', function(req, res, next) {
  item.aggregate([
    { 
      "$match": { 
        "date" : { 
          "$gt" : moment().subtract(30, 'days').startOf('day').toDate(),
          "$lt" : moment().endOf('day').toDate()
        },
        "mactive": true
      }
    },
    {
      "$project": { 
        "title": 1, 
        "date": 1,
        "link": 1,
        "point": "$mpoint",
        "items": "$mitem"
      }
    },
    {$sort: { point : -1}}
  ])
  .limit(30)
  .exec(function(err, docs){
    console.log(docs);
    res.render('index', { 
      title: 'Month',
      docs: docs
    });
  })
});

module.exports = router;
