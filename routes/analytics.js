var express = require('express');
var mongoose = require('mongoose');
var async = require('async');
var moment = require('moment');
var _ = require('underscore');
var router = express.Router();

// models
var rss = require( '../models/rss' );
var item = require( '../models/item' );

/* day */
router.get('/day', function(req, res, next) {
  async.waterfall([
    // Step 1: Get all item within a day
    function(callback){
      item.find({
        "date" : { 
          "$gt" : moment().startOf('day').toDate(),
          "$lt" : moment().endOf('day').toDate()
        }
      })
      .exec(function(err, docs){
        callback(err,docs);
      })
    },
    // Step 2: Update item with related item
    function(docs, callback){
      async.each(docs,function(doc,cb){
        item.aggregate([
          { 
            "$match": { 
              "$text": { "$search": doc.title },
              "date" : { 
                "$gt" : moment().startOf('day').toDate(),
                "$lt" : moment().endOf('day').toDate()
              }
            } 
          },
          { 
           "$project": { 
              "title": 1, 
              "score": { "$meta": "textScore" },
              "parent": 1,
              "date": 1,
              "link": 1
            } 
          },
          { 
            "$match": { 
              "score": { "$gt": 2 } ,
              "parent": { "$ne": doc.parent },
              "date" : { 
                "$gt" : moment().startOf('day').toDate(),
                "$lt" : moment().endOf('day').toDate()
              }
            } 
          },
          { $sort : { "score": -1 } }
        ])
        .exec(function(err, results) {
          // Update current item with related content
          item.findByIdAndUpdate(doc._id, { dpoint: _.size(results), ditem: results },function(err,aaa){
            cb(err);
          })
        });
      }, function(err) {
        callback(err);
      })
    },
    function(callback){
      // Step 3: Remove Duplicated item
      item.find({
        "date" : { 
          "$gt" : moment().startOf('day').toDate(),
          "$lt" : moment().endOf('day').toDate()
        },
        "dpoint": { "$gt" : 0 },
        "dactive": true
      })
      .select("title dpoint ditem dactive")
      .sort({ "dpoint": -1 })
      .exec(function(err, docs){
        // Step 3.1: Remove Duplicated item and Set them inactive
        async.eachSeries(docs,function(doc, cb){
          doc.ditem = _(doc.ditem).chain().map(function(o){
            return _.pick(o, '_id');
          }).flatten().pluck('_id').map(function(id){
            return mongoose.Types.ObjectId(id);
          }).unique().value()   
          
          // DB works
          item.findById(mongoose.Types.ObjectId(doc._id)).select("_id title dactive")
          .exec(function(error,res){
            if(res.dactive == true){ // Don't update it when it already set to false
              item.update({ "_id": { "$in" : doc.ditem }, "dactive": true }, {  "dactive" : false }, { multi: true }, function(err,rels){
              cb(err);
            })  
            }else{
              cb(null);
            }
          })
            
        },function(err){
          callback(err);        
        })
      })
    }
  ], function(err) {
    res.send("Day Done");
  })
});

/* week */
router.get('/week', function(req, res, next) {
  async.waterfall([
    // Step 1: Get all item within a week
    function(callback){
      item.find({
        "date" : { 
          "$gt" : moment().subtract(7, 'days').startOf('day').toDate(),
          "$lt" : moment().endOf('day').toDate()
        }
      })
      .exec(function(err, docs){
        callback(err,docs);
      })
    },
    // Step 2: Update item with related item
    function(docs, callback){
      async.each(docs,function(doc,cb){
        item.aggregate([
          { 
            "$match": { 
              "$text": { "$search": doc.title },
              "date" : { 
                "$gt" : moment().subtract(7, 'days').startOf('day').toDate(),
                "$lt" : moment().endOf('day').toDate()
              }
            } 
          },
          { 
           "$project": { 
              "title": 1, 
              "score": { "$meta": "textScore" },
              "parent": 1,
              "date": 1,
              "link": 1
            } 
          },
          { 
            "$match": { 
              "score": { "$gt": 2 } ,
              "parent": { "$ne": doc.parent },
              "date" : { 
                "$gt" : moment().subtract(30, 'days').startOf('day').toDate(),
                "$lt" : moment().endOf('day').toDate()
              }
            } 
          },
          { $sort : { "score": -1 } }
        ])
        .exec(function(err, results) {
          // Update current item with related content
          item.findByIdAndUpdate(doc._id, { wpoint: _.size(results), witem: results },function(err,aaa){
            cb(err);
          })
        });
      }, function(err) {
        callback(err);
      })
    },
    function(callback){
      // Step 3: Remove Duplicated item
      item.find({
        "date" : { 
          "$gt" : moment().subtract(30, 'days').startOf('day').toDate(),
          "$lt" : moment().endOf('day').toDate()
        },
        "wpoint": { "$gt" : 0 },
        "wactive": true
      })
      .select("title wpoint witem wactive")
      .sort({ "wpoint": -1 })
      .exec(function(err, docs){
        // Step 3.1: Remove Duplicated item and Set them inactive
        async.eachSeries(docs,function(doc, cb){
          doc.witem = _(doc.witem).chain().map(function(o){
            return _.pick(o, '_id');
          }).flatten().pluck('_id').map(function(id){
            return mongoose.Types.ObjectId(id);
          }).unique().value()   
          
          // DB works
          item.findById(mongoose.Types.ObjectId(doc._id)).select("_id title wactive")
          .exec(function(error,res){
            if(res.wactive == true){ // Don't update it when it already set to false
              item.update({ "_id": { "$in" : doc.witem }, "wactive": true }, {  "wactive" : false }, { multi: true }, function(err,rels){
              cb(err);
            })  
            }else{
              cb(null);
            }
          })
            
        },function(err){
          callback(err);        
        })
      })
    }
  ], function(err) {
    res.send("Week Done");
  })
});


/* month */
router.get('/month', function(req, res, next) {
  async.waterfall([
    // Step 1: Get all item within a month
    function(callback){
      item.find({
        "date" : { 
          "$gt" : moment().subtract(30, 'days').startOf('day').toDate(),
          "$lt" : moment().endOf('day').toDate()
        }
      })
      .exec(function(err, docs){
        callback(err,docs);
      })
    },
    // Step 2: Update item with related item
    function(docs, callback){
      async.each(docs,function(doc,cb){
        item.aggregate([
          { 
            "$match": { 
              "$text": { "$search": doc.title },
              "date" : { 
                "$gt" : moment().subtract(30, 'days').startOf('day').toDate(),
                "$lt" : moment().endOf('day').toDate()
              }
            } 
          },
          { 
           "$project": { 
              "title": 1, 
              "score": { "$meta": "textScore" },
              "parent": 1,
              "date": 1,
              "link": 1
            } 
          },
          { 
            "$match": { 
              "score": { "$gt": 2 } ,
              "parent": { "$ne": doc.parent },
              "date" : { 
                "$gt" : moment().subtract(30, 'days').startOf('day').toDate(),
                "$lt" : moment().endOf('day').toDate()
              }
            } 
          },
          { $sort : { "score": -1 } }
        ])
        .exec(function(err, results) {
          // Update current item with related content
          item.findByIdAndUpdate(doc._id, { mpoint: _.size(results), mitem: results },function(err,aaa){
            cb(err);
          })
        });
      }, function(err) {
        callback(err);
      })
    },
    function(callback){
      // Step 3: Remove Duplicated item
      item.find({
        "date" : { 
          "$gt" : moment().subtract(30, 'days').startOf('day').toDate(),
          "$lt" : moment().endOf('day').toDate()
        },
        "mpoint": { "$gt" : 0 },
        "mactive": true
      })
      .select("title mpoint mitem mactive")
      .sort({ "mpoint": -1 })
      .exec(function(err, docs){
        // Step 3.1: Remove Duplicated item and Set them inactive
        async.eachSeries(docs,function(doc, cb){
          doc.mitem = _(doc.mitem).chain().map(function(o){
            return _.pick(o, '_id');
          }).flatten().pluck('_id').map(function(id){
            return mongoose.Types.ObjectId(id);
          }).unique().value()   
          
          // DB works
          item.findById(mongoose.Types.ObjectId(doc._id)).select("_id title mactive")
          .exec(function(error,res){
            if(res.mactive == true){ // Don't update it when it already set to false
              item.update({ "_id": { "$in" : doc.mitem }, "mactive": true }, {  "mactive" : false }, { multi: true }, function(err,rels){
              cb(err);
            })  
            }else{
              cb(null);
            }
          })
            
        },function(err){
          callback(err);        
        })
      })
    }
  ], function(err) {
    res.send("Done");
  })
});


module.exports = router;
