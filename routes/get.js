var express = require('express');
var router = express.Router();
var async = require('async');
var parser = require('node-feedparser');
var request = require('request');

// models
var rss = require( '../models/rss' );
var item = require( '../models/item' );

/* Initialize the database */
router.get('/', function(req, res, next) {
  var rssSource = [
    {
      url: "http://macrumors.com/",
      link: "http://feeds.macrumors.com/MacRumors-All?format=xml",
      cat: ['Tech', 'Apple']
    },
    {
      url: "https://9to5mac.com/",
      link: "https://9to5mac.com/feed/",
      cat: ['Tech', 'Apple']
    },
    {
      url: "http://www.theverge.com/",
      link: "http://www.theverge.com/apple/rss/index.xml",
      cat: ['Tech', 'Apple']
    },
    {
      url: "http://cnet.com/",
      link: "http://feed.cnet.com/feed/collections/apple",
      cat: ['Tech', 'Apple']
    },
    {
      url: "http://appleinsider.com/",
      link: "http://appleinsider.com/appleinsider.rss",
      cat: ['Tech', 'Apple']
    },
    {
      url: "http://www.macworld.co.uk/",
      link: "http://www.macworld.co.uk/latest/rss",
      cat: ['Tech', 'Apple']
    },
    {
      url: "https://flipboard.com/",
      link: "https://flipboard.com/topic/apple.rss",
      cat: ['Tech', 'Apple']
    },
    {
      url: "http://www.cultofmac.com/",
      link: "http://www.cultofmac.com/feed/",
      cat: ['Tech', 'Apple']
    },
    {
      url: "http://www.reddit.com/",
      link: "https://www.reddit.com/r/apple.rss",
      cat: ['Tech', 'Apple']
    },
    {
      url: "http://www.iclarified.com/",
      link: "http://www.iclarified.com/rss/rss.xml",
      cat: ['Tech', 'Apple']
    }
  ];
  
  rss.count(function (err, count) {
    if (!err && count === 0) {
      async.each(rssSource, function(rssItem, callback) {
        var newRss = rss(rssItem).save(function(err) {
          if(err) console.log(err);
          callback()
        });
      },function(err){
        res.send("Done");
      });
    }else{
      res.send("Done");
    }
  });
});

router.get('/feed', function(req, res, next) {
  var getFeedContent = function(feedUrl, callback) {
    request(feedUrl, function (error, response, body) {
      if (error || response.statusCode != 200){
        callback(true);
      }else{
        callback(null, body);          
      }
    });
  }
  var parseFeedContent = function(body, callback) {
    parser(body,function(error, res){
      callback(error, res);
    });
  }
  
  rss.find().exec(function(err, docs){    
    async.each(docs, function(doc){
      async.waterfall([
        function(callback){
          callback(null, doc.link);
        },
        getFeedContent, 
        parseFeedContent  
      ],function(err, result) {
        if ( result && result.items ){
          async.each(result.items, function(feed, callback) {
            feed.parent = doc._id;
            var newItem = item(feed).save();
            callback()
          });
        }else{
          res.send("Done");
        }
      })
    },function(err){
      res.send("Done");
    })
  })
});

router.get('/analytics', function(req, res, next) {
  item.find().exec(function(err, docs){
    async.each(docs,function(doc,callback){
      item.aggregate([
        { 
          "$match": { 
            "$text": { 
              "$search": doc.title 
            } 
          } 
        },
        { 
         "$project": { 
            "title": 1, 
            "score": { 
               "$meta": "textScore" 
            },
            "parent": 1
          } 
        },
        { 
          "$match": { 
            "score": { "$gt": 2 } ,
            "parent": { "$ne": doc.parent }
          } 
        },
        {$sort: { score : -1}}
      ])
      .exec(function(err, results) {
        doc.rels = results;
        callback(null,doc);
      });
    }, function(err) {
      res.render('analytics', { 
        title: 'Express',
        docs: docs
      });
    })
  })
})

module.exports = router;
