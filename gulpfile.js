var gulp = require('gulp'),
    compass = require('gulp-compass'),
    jshint = require('gulp-jshint'),
    livereload = require('gulp-livereload'),
    browserSync = require('browser-sync'),
    nodemon = require('gulp-nodemon');

gulp.task('compass-task', function () {
    return gulp.src('public/sass/*.scss').pipe(compass({
                    css: 'public/css',
                    sass: 'public/sass',
                    style: 'compact',
                    comments: false
                }));
});

gulp.task('watch', function () {
    // compile compass to css then minify all css
    gulp.watch('./public/sass/*.scss', ['compass-task']);

    livereload.listen();
});

gulp.task('browser-sync', ['nodemon-task'], function () {
    browserSync.init(null, {
        proxy: "http://localhost:3000",
        files: ["public/**/*.*","views/*.jade"],
        browser: "google chrome",
        port: 7000
    });
});

gulp.task('nodemon-task', function () {
    return nodemon({
        script: 'app.js'
    });
});

gulp.task('default', ['watch', 'browser-sync']);
